# Electrum-Cash Checkpoint

Exports a checkpoints that can be used together with a merkle proof to determine if a given block is a part of the current blockchain.