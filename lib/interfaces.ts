export interface Checkpoint
{
	// The height in the blockchain the checkpoint is for.
	height: number;

	// The merkle root hash of all blocks in the blockchain up to and including the checkpoint height.
	merkleRoot: string;
}
