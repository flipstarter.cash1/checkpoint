// Export the checkpoint.
export const electrumCheckpoint =
{
	// Set the block height for the checkpoint.
	height: 800000,

	// Set the root hash for the merkle tree of all blocks included up to and including the block height.
	merkleRoot: '7b82ed1bcf624b28f3d7cd6acecce5422d6c10584978cffd0307603858d713f9',
};
